# Translation of KVIrc logview messages to Portuguese
# Copyright (C) 2010 the KVIrc's copyright holder
# This file is distributed under the same license as the KVirc package.
#
# Zé <ze@mandriva.org>, 2004, 2006, 2007.
# Zé <mmodem00@gmail.com>, 2010.
# Rafael_C_V <rafaelvizioni@ig.com.br> - PT-BR - 2011
msgid ""
msgstr ""
"Project-Id-Version: kvirc_logview\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-04-10 15:55+0200\n"
"PO-Revision-Date: 2011-06-15 23:04-0300\n"
"Last-Translator: Rafael <rafaelvizioni@ig.com.br>\n"
"Language-Team: Portuguese <kde-i18n-doc@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/modules/logview/LogViewWindow.cpp:356
#, fuzzy
msgid "%1 on %2"
msgstr "%1 em %2"

#: src/modules/logview/LogViewWindow.cpp:172
msgid "Apply filter"
msgstr "Aplicar filtro"

#: src/modules/log/libkvilog.cpp:118
msgid "Can't log to file '%1'"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:103
msgid "Cancel"
msgstr ""

#: src/modules/logview/LogViewWidget.cpp:69
msgid "Channel"
msgstr "Canal"

#: src/modules/logview/LogViewWindow.cpp:636
msgid "Channel %1 on %2"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:461
msgid "Confirm current user log deletion"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:477
msgid "Confirm current user logs deletion"
msgstr ""

#: src/modules/logview/LogViewWidget.cpp:81
msgid "Console"
msgstr "Consola"

#: src/modules/logview/LogViewWindow.cpp:639
msgid "Console on %1"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:141
msgid "Contents filter"
msgstr "Filtro de conteúdos"

#: src/modules/logview/LogViewWidget.cpp:77
msgid "DCC Chat"
msgstr "Conversa DCC"

#: src/modules/logview/LogViewWindow.cpp:645
msgid "DCC Chat with %1"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:478
msgid "Do you really wish to delete all these logs?"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:462
msgid "Do you really wish to delete this log?"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:777
msgid "Export Log - KVIrc"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:442
msgid "Export log file to"
msgstr ""

#: src/modules/log/libkvilog.cpp:323
msgid "Failed to export the log '%1'"
msgstr ""

#: src/modules/log/libkvilog.cpp:313
msgid "Failed to load logview module, aborting"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:777
msgid "File '%1' already exists. Do you want to overwrite it?"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:116
msgid "Filter"
msgstr "Filtro"

#: src/modules/logview/LogViewWindow.cpp:191
msgid "HTML archive"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:108
msgid "Index"
msgstr "Índice"

#: src/modules/logview/LogViewWindow.cpp:68
msgid "Log File"
msgstr "Arquivo de Registo"

#: src/modules/logview/LogViewWindow.cpp:235
msgid "Log Viewer"
msgstr "Visualizador de Logs"

#: src/modules/logview/LogViewWindow.cpp:150
msgid "Log contents mask:"
msgstr "Máscara de conteúdos do registo:"

#: src/modules/logview/LogViewWindow.cpp:144
msgid "Log name mask:"
msgstr "Máscara de nome do registo:"

#: src/modules/log/libkvilog.cpp:107
#: src/modules/log/libkvilog.cpp:161
#: src/modules/log/libkvilog.cpp:211
msgid "Missing window id after the 'w' switch"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:164
#, fuzzy
msgid "Only newer than"
msgstr "Apenas mais recente que"

#: src/modules/logview/LogViewWindow.cpp:156
msgid "Only older than"
msgstr "Apenas mais antigo que"

#: src/modules/logview/LogViewWidget.cpp:85
msgid "Other"
msgstr "Outro"

#: src/modules/logview/LogViewWidget.cpp:73
msgid "Query"
msgstr "Pesquisar"

#: src/modules/logview/LogViewWindow.cpp:642
msgid "Query with %1 on %2"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:440
msgid "Remove all log files within this folder"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:443
#, fuzzy
msgid "Remove log file"
msgstr "Remover arquivo"

#: src/modules/logview/LogViewWindow.cpp:132
msgid "Show DCC chat logs"
msgstr "Mostrar logs de conversas DCC"

#: src/modules/logview/LogViewWindow.cpp:120
msgid "Show channel logs"
msgstr "Mostrar logs de canal"

#: src/modules/logview/LogViewWindow.cpp:128
msgid "Show console logs"
msgstr "Mostrar logs de console"

#: src/modules/logview/LogViewWindow.cpp:136
msgid "Show other logs"
msgstr "Mostrar outros logs"

#: src/modules/logview/LogViewWindow.cpp:124
msgid "Show query logs"
msgstr "Mostrar logs de privado"

#: src/modules/logview/LogViewWindow.cpp:648
msgid "Something on %1"
msgstr ""

#: src/modules/log/libkvilog.cpp:120
msgid "This window has no logging capabilities"
msgstr ""

#: src/modules/log/libkvilog.cpp:103
#: src/modules/log/libkvilog.cpp:157
#: src/modules/log/libkvilog.cpp:207
msgid "Window '%1' not found"
msgstr ""

#: src/modules/log/libkvilog.cpp:265
msgid "Window with id '%1' not found, returning empty string"
msgstr ""

#: src/modules/logview/LogViewWindow.cpp:190
#, fuzzy
msgid "plain text file"
msgstr "Filtro de conteúdos"

#~ msgid "Abort filtering"
#~ msgstr "Abortar filtragem"

#~ msgid "Browse &Log Files"
#~ msgstr "Procurar Arquivos de &Registo"

#~ msgid "Filtering files..."
#~ msgstr "A filtrar arquivos..."

#~ msgid "Remove all these channel/query log files"
#~ msgstr "Remover todos estes logs de canais/privados"
