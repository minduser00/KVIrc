# translation of sharedfileswindow_fr.po to Français
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Ahinu <ahinu@wanadoo.fr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: sharedfileswindow_fr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-04-10 15:55+0200\n"
"PO-Revision-Date: 2010-10-31 14:45+0100\n"
"Last-Translator: ealexp <ealexp@ealexp.eu>\n"
"Language-Team: Français <kde-francophone@kde.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"

#: src/modules/sharedfile/libkvisharedfile.cpp:232
#, c-format
msgid "    Expires in %d hours %d minutes %d seconds"
msgstr "    Expires dans %d heure(s) %d minute(s) %d seconde(s)"

#: src/modules/sharedfile/libkvisharedfile.cpp:221
#, c-format
msgid "    File: %s (%u bytes)"
msgstr "    Fichier : %s (%u octets)"

#: src/modules/sharedfile/libkvisharedfile.cpp:223
#, c-format
msgid "    Mask: %s"
msgstr "    Masque : %s"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:242
msgid "&Add..."
msgstr "&Ajouter..."

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:75
msgid "&Browse..."
msgstr "&Naviguer..."

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:246
msgid "&Edit"
msgstr "&Editer"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:97
msgid "&OK"
msgstr "&Valider"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:177
msgid "Can't open the file"
msgstr "Impossible d'ouvrir le fichier"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:101
msgid "Cancel"
msgstr "Annuler"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:59
msgid "Edit Shared File - KVIrc"
msgstr "Éditeur de fichier - KVIrc"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:86
msgid "Expire at:"
msgstr "Expire le :"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:226
msgid "Expires"
msgstr "Expire"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:68
msgid "File path:"
msgstr "Chemin du fichier :"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:223
msgid "Filename"
msgstr "Nom de fichier"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:159
msgid "Invalid expire time"
msgstr "Date d'expiration invalide"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:168
msgid "Invalid share name"
msgstr "Nom de partage invalide"

#: src/modules/sharedfile/libkvisharedfile.cpp:101
msgid "Invalid timeout, ignoring"
msgstr "Temps d'expiration invalide, ignoré"

#: src/modules/sharedfile/libkvisharedfile.cpp:115
#, fuzzy
msgid "Invalid visible name: using default"
msgstr "Nom de fichier invalide : utilisation d'un nom par défaut "

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:224
msgid "Mask"
msgstr "Masque"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:222
msgid "Name"
msgstr "Nom"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:199
msgid "Never"
msgstr "Jamais"

#: src/modules/sharedfile/libkvisharedfile.cpp:242
msgid "No active file sharedfile"
msgstr "Pas de fichier partagé actif"

#: src/modules/sharedfile/libkvisharedfile.cpp:159
msgid "No sharedfile with visible name '%Q' and user mask '%Q'"
msgstr "Aucun fichier partagé dont le nom est '%Q' et le masque '%Q'"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:162
#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:170
#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:179
msgid "OK"
msgstr "OK"

#: src/modules/sharedfile/libkvisharedfile.cpp:121
msgid "Ops..failed to add the sharedfile..."
msgstr "Oups... impossible d'ajouter le fichier au partage..."

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:244
msgid "Re&move"
msgstr "Suppri&mer"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:61
msgid "Share name:"
msgstr "Nom de partage :"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:695
msgid "Shared Files"
msgstr "Fichiers partagés"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:160
msgid ""
"The expire date/time is in the past: please either remove the \"expires"
"\"check mark or specify a expire date/time in the future"
msgstr ""
"La date d'expiration est dans le passé : veuillez soit décocher \"Expire\" "
"soit spécifier une date dans le future"

#: src/modules/sharedfile/libkvisharedfile.cpp:85
msgid "The file '%Q' is not readable"
msgstr "Le fichier '%Q', n'est pas lisible"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:178
msgid "The file doesn't exist or it is not readable, please check the path"
msgstr "Le fichier n'existe pas ou est illisible, vérifiez le chemin"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:169
msgid "The share name can't be empty, please correct it"
msgstr "Le nom de partage ne peut pas être vide !"

#: src/modules/sharedfile/libkvisharedfile.cpp:243
#, c-format
msgid "Total: %d sharedfile"
msgstr "Total : %d fichier(s) partagé(s)"

#: src/modules/sharedfileswindow/SharedFilesWindow.cpp:79
msgid "User mask:"
msgstr "Masque utilisateur :"

#~ msgid "Manage S&hared Files"
#~ msgstr "Gérer les fichiers p&artagés"
